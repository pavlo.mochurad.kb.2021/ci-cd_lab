FROM python:3.9

ARG GROUP_NAME

COPY . /app

WORKDIR /app/simple_python_app

RUN pip install -r ../requirements.txt

ENV FLASK_DEBUG=true
ENV DATABASE_NAME=KB-308

CMD [ "python", "app.py" ]