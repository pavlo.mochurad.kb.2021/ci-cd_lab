import sqlite3

def check_data(db_name):
    connector = sqlite3.connect(f"{db_name}.db")
    mydb = connector.cursor()

    mydb.execute("SELECT * FROM Students")
    rows = mydb.fetchall()

    if len(rows) > 0:
        print("Дані в базі даних:")
        for row in rows:
            print(row)
    else:
        print("Таблиця Students порожня")

    connector.close()

# Замість 'db_name' введіть назву вашої бази даних SQLite3 без розширення '.db'
db_name = input("DB:")
check_data(db_name)