import sqlite3

data = [
    ("Михайло", "Баленко", "example@gmail.com"),
    ("Данило", "Гавриленко", "example@gmail.com"),
    ("Анастасія", "Гриджук", "example@gmail.com"),
    ("Максим", "Дідух", "example@gmail.com"),
    ("Володимир", "Долошицький", "example@gmail.com"),
    ("Вікторія", "Закорчменна", "example@gmail.com"),
    ("Тарас", "Іваночко", "example@gmail.com"),
    ("Іван", "Кіщак", "example@gmail.com"),
    ("Богдан", "Коваль", "example@gmail.com"),
    ("Катерина", "Коваль", "example@gmail.com"),
    ("Максим", "Копильчак", "example@gmail.com"),
    ("Олександр", "Королевич", "example@gmail.com"),
    ("Максим", "Куспись", "example@gmail.com"),
    ("Юрій", "Магола", "example@gmail.com"),
    ("Денис", "Михальський", "example@gmail.com"),
    ("Максим", "Моспанко", "example@gmail.com"),
    ("Павло", "Мочурад", "example@gmail.com"),
    ("Степан", "Надь", "example@gmail.com"),
    ("Богдан", "Оніщенко", "example@gmail.com"),
    ("Олег", "Пліхтяк", "example@gmail.com"),
    ("Тарас", "Попович", "example@gmail.com"),
    ("Роман", "Русін", "example@gmail.com"),
    ("Ілля", "Таранюк", "example@gmail.com"),
    ("Христина", "Теренчин", "example@gmail.com"),
    ("Марко", "Тураш", "example@gmail.com"),
    ("Діана", "Цебенко", "example@gmail.com"),
    ("Ярослав", "Чубка", "example@gmail.com")
]


def setup_db(db_name):
    connector = sqlite3.connect(f"{db_name}.db")
    mydb = connector.cursor()
    mydb.execute(f"CREATE TABLE IF NOT EXISTS Students (id INTEGER PRIMARY KEY, first_name VARACHAR, last_name VARACHAR, email VARACHAR);")

    mydb.execute("SELECT COUNT(*) FROM Students")
    count = mydb.fetchone()[0]
    if count == 0:
        for item in data:
            mydb.execute("INSERT INTO Students (first_name, last_name, email) VALUES (?, ?, ?);", item[0:3])

    # Save (commit) the changes
    connector.commit()
    # Close the connection
    connector.close()


